# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Python imports
import requests
import json
from requests_oauthlib import OAuth1
from urlparse import urljoin, urlparse, parse_qs

# Third party imports

# Local imports

__author__ = 'jon.caldering@gmail.com'


class WpRest(object):
    """
    Wrapper of the WordPress REST Library
    """
    def __init__(self, server_url, client_key=None, client_secret=None, username=None, password=None):
        """
        Is possible to login by basic authentication or by Oauth (there is a bug with the Oauth)
        :param server_url:
        :param client_key:
        :param client_secret:
        :param username:
        :param password:
        """
        self.server_url = server_url
        self.client_key = client_key
        self.client_secret = client_secret
        self.session = requests.Session()
        if self.client_key is not None and self.client_secret is not None:
            self.oauth_request, self.oauth_authorize, self.oauth_access = self.get_oauth_urls()
            self.session.auth = self.oauth_flow()
        if username is not None and password is not None:
            self.session.auth = (username, password)
        # SSL Verify
        self.session.verify = False

    def get_oauth_urls(self):
        """
        From the WP api we get just the path, because we already have the server url
        :return:
        """
        res = self.__request('GET', '/wp-json')
        dict_res = json.loads(res.text)
        oauth_request = urlparse(dict_res['authentication']['oauth1']['request']).path
        oauth_authorize = urlparse(dict_res['authentication']['oauth1']['authorize']).path
        oauth_access = urlparse(dict_res['authentication']['oauth1']['access']).path
        return oauth_request, oauth_authorize, oauth_access

    def oauth_flow(self):
        """
        Three legs in the OAuth loggin until get the auth with the right tokens
        :return:
        """
        # Request
        oauth = OAuth1(self.client_key, client_secret=self.client_secret, signature_type='body')
        res = self.__request('post', self.oauth_request, auth=oauth)
        credentials = parse_qs(res.content)
        resource_owner_key = credentials.get('oauth_token')[0]
        resource_owner_secret = credentials.get('oauth_token_secret')[0]
        # Access
        url = urljoin(self.server_url, self.oauth_authorize) + '?oauth_token=' + resource_owner_key
        print('Please go here and authorize: {url}'.format(url=url))
        verifier = raw_input('Please input the verifier: ')
        # Authorize
        oauth = OAuth1(self.client_key,
                       client_secret=self.client_secret,
                       resource_owner_key=resource_owner_key,
                       resource_owner_secret=resource_owner_secret,
                       verifier=verifier, signature_type='body')
        res = self.__request('post', self.oauth_access, auth=oauth)
        credentials = parse_qs(res.content)
        new_resource_owner_key = credentials.get('oauth_token')[0]
        new_resource_owner_secret = credentials.get('oauth_token_secret')[0]
        # New Auth
        return OAuth1(self.client_key,
                      client_secret=self.client_secret,
                      resource_owner_key=new_resource_owner_key,
                      resource_owner_secret=new_resource_owner_secret,
                      signature_type='body')

    def __request(self, method, endpoint, **kwargs):
        """
        All requests calls come here, unified
        :param method:
        :param endpoint:
        :param kwargs:
        :return:
        """
        url = urljoin(self.server_url, endpoint)
        res = self.session.request(method, url, **kwargs)
        return res

    def create_post(self, post_data):
        """
        Create a new post
        http://v2.wp-api.org/reference/posts/
        :param post_data:
        :return:
        """
        return self.__request('POST', 'wp-json/wp/v2/posts', data=post_data)

    def list(self, params=None):
        """
        List all posts
        http://v2.wp-api.org/reference/posts/
        :return:
        """
        return self.__request('GET', 'wp-json/wp/v2/posts', params=params)

    def get(self, post_id):
        """
        Get a certain post
        http://v2.wp-api.org/reference/posts/
        :param post_id:
        :return:
        """
        return self.__request('GET', 'wp-json/wp/v2/posts/{id}'.format(id=post_id))

    def delete(self, post_id):
        """
        Delete a certain post
        :param post_id:
        :return:
        """
        return self.__request('DELETE', 'wp-json/wp/v2/posts/{id}'.format(id=post_id))
