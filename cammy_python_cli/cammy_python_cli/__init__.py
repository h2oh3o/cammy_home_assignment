# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Python imports
import argparse
import sys
import textwrap
from os.path import expanduser, isfile, join
from os import getenv
import json
# Third party imports

# Local imports
from wp_rest import WpRest

__author__ = 'jon.caldering@gmail.com'


def pretty_json(data):
    try:
        result = json.dumps(data, sort_keys=True, indent=4, separators=(',', ': '))
    except Exception:
        result = json.dumps(json.loads(data), sort_keys=True, indent=4, separators=(',', ': '))
    return result


class DefaultHelpParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write(bytes('error: {message}\n'.format(message=message).encode('utf-8')))
        self.print_help()
        sys.exit(2)


def main():
    global_parser = DefaultHelpParser(add_help=False, formatter_class=argparse.RawDescriptionHelpFormatter,
                                      description=textwrap.dedent('''\
    Manage WordPress posts
        Credentials are given in the next order:
            Arguments (--url, --username --password)
            Environment variables (CAMMY_CLI_URL, CAMMY_CLI_USERNAME, CAMMY_CLI_PASSWORD
            Json config with keys url, username and password in file: ~/.cammy_cli_config'''))
    # Global options
    global_parser.add_argument("--url", type=str, help="URL of the WordPress API", required=False)
    global_parser.add_argument("--username", type=str, help="Username for WordPress", required=False)
    global_parser.add_argument("--password", type=str, help="Password for WordPress", required=False)
    global_parser.add_argument("-v", "--verbosity", action="count", help="Verbosity of responses (-v, -vv)",
                               required=False, default=0)

    # Subparsers
    parser = DefaultHelpParser()
    subparsers = parser.add_subparsers(dest="main_opt")

    # Create post
    parser_create_post = subparsers.add_parser('create', help="Create a new blog post", parents=[global_parser])
    create_group = parser_create_post.add_mutually_exclusive_group(required=True)
    # Create options
    create_group.add_argument('--json', metavar="STRING", type=str)
    create_group.add_argument('--file', metavar="FILEPATH", type=file)

    # Delete post
    parser_delete_post = subparsers.add_parser('delete', help="Delete a blog post", parents=[global_parser])
    parser_delete_post.add_argument('blog_id', type=int, help="Identifier of the blog to delete")

    # Retrieve a post
    parser_retrieve_post = subparsers.add_parser('get', help="Get a single blog post", parents=[global_parser])
    parser_retrieve_post.add_argument('blog_id', type=int, help="Identifier of the blog to retrieve")

    # List all posts
    parser_list_post = subparsers.add_parser('list', help="List all blog posts", parents=[global_parser])
    parser_list_post.add_argument("--context", type=str, choices=["view", "embed", "edit"],
                                  help="Scope under which the request is made; determines fields present in response.")
    parser_list_post.add_argument("--page", type=int,
                                  help="Current page of the collection.")
    parser_list_post.add_argument("--per_page", type=int,
                                  help="Maximum number of items to be returned in result set.")
    parser_list_post.add_argument("--search", type=str,
                                  help="Limit results to those matching a string.")
    parser_list_post.add_argument("--after",
                                  help="Limit response to resources published after a given ISO8601 compliant date.")
    parser_list_post.add_argument("--author",
                                  help="Limit result set to posts assigned to specific authors.")
    parser_list_post.add_argument("--author_exclude",
                                  help="Ensure result set excludes posts assigned to specific authors.")
    parser_list_post.add_argument("--before",
                                  help="Limit response to resources published before a given ISO8601 compliant date.")
    parser_list_post.add_argument("--exclude",
                                  help="Ensure result set excludes specific ids.")
    parser_list_post.add_argument("--include",
                                  help="Limit result set to specific ids.")
    parser_list_post.add_argument("--offset",
                                  help="Offset the result set by a specific number of items.")
    parser_list_post.add_argument("--order", choices=["asc", "desc"],
                                  help="Order sort attribute ascending or descending.")
    parser_list_post.add_argument("--orderby", choices=["date", "id", "include", "title", "slug"],
                                  help="Sort collection by object attribute.")
    parser_list_post.add_argument("--slug",
                                  help="Limit result set to posts with a specific slug.")
    parser_list_post.add_argument("--status",
                                  help="Limit result set to posts assigned a specific status.")
    parser_list_post.add_argument("--filter",
                                  help="Use WP Query arguments to modify the response; private query vars require appropriate authorization.")
    parser_list_post.add_argument("--categories",
                                  help="Limit result set to all items that have the specified term assigned in the categories taxonomy.")
    parser_list_post.add_argument("--tags",
                                  help="Limit result set to all items that have the specified term assigned in the tags taxonomy.")
    # Create config file
    parser_create_config_file = subparsers.add_parser('create_config', help="Create config file in home")
    parser_create_config_file.add_argument("url", type=str, help="URL of the WordPress API")
    parser_create_config_file.add_argument("username", type=str, help="Username for WordPress")
    parser_create_config_file.add_argument("password", type=str, help="Password for WordPress")
    arguments = parser.parse_args()

    config_file_path = join(expanduser("~"), '.cammy_cli_config')


    def get_config_file():
        with open(config_file_path) as config_file:
            try:
                config = json.load(config_file)
            except ValueError as e:
                print("The config file is not a valid JSON")
                sys.exit(2)
            return config


    # Create config file
    if arguments.main_opt == "create_config":
        config_file = open(config_file_path, 'w')
        json.dump({"url": arguments.url, "username": arguments.username, "password": arguments.password}, config_file)
        config_file.close()
        print("Config file has been written successfully mate. You can find it in {}".format(config_file_path))
        sys.exit(0)

    # Getting URL
    url = None
    if arguments.url is None:
        # if "CAMMY_CLI_URL" not in getenv:
        if getenv('CAMMY_CLI_URL') is None:
            if isfile(config_file_path):
                config = get_config_file()
                if 'url' in config:
                    url = config['url']
        else:
            url = getenv('CAMMY_CLI_URL')
    else:
        url = arguments.url

    # Getting Username
    username = None
    if arguments.username is None:
        if getenv('CAMMY_CLI_USERNAME') is None:
            if isfile(config_file_path):
                config = get_config_file()
                if 'username' in config:
                    username = config['username']
        else:
            username = getenv('CAMMY_CLI_USERNAME')
    else:
        username = arguments.username

    # Getting Password
    password = None
    if arguments.password is None:
        if getenv('CAMMY_CLI_PASSWORD') is None:
            if isfile(config_file_path):
                config = get_config_file()
                if 'password' in config:
                    password = config['password']
        else:
            password = getenv('CAMMY_CLI_PASSWORD')
    else:
        password = arguments.password

    if url is None or username is None or password is None:
        print("ERROR: The URL and credentials of the WordPress API have to be defined.")
        print("\tURL: {}".format(url))
        print("\tUsername: {}".format(username))
        print("\tPassword: {}".format(password))
        sys.exit(2)

    wp_rest = WpRest(url, username=username, password=password)

    # Create
    if arguments.main_opt == "create":
        if arguments.file is not None:
            try:
                post_create = json.load(arguments.file)
            except Exception as e:
                print("There is a problem parsing the file as a json, check the file: {}".format(arguments.file))
                print(e)
                sys.exit(2)
        elif arguments.json is not None:
            try:
                post_create = json.loads(arguments.json)
            except Exception as e:
                print("There is a problem parsing the json, please check it:\n{}".format(arguments.json))
                print(e)
                sys.exit(2)
        else:
            print("That's weird, argparse should be controlling an input of FILE or JSON but not neither. Report this error")
            sys.exit(2)
        creation = wp_rest.create_post(post_create)
        if creation.status_code in [200, 201]:
            print("Post created")
        else:
            print("There were an error creating the post")
            print(creation.text)
            print(creation)


    # List
    if arguments.main_opt == 'list':
        list_arguments = {}
        list_ops = ['context', 'page', 'per_page', 'search', 'after', 'author', 'author_exclude', 'before', 'exclude',
                    'include', 'offset', 'order', 'orderby', 'slug', 'status', 'filter', 'categories', 'tags']
        for opt in list_ops:
            argument = getattr(arguments, opt)
            if argument is not None:
                list_arguments.update({opt: argument})
        list_result = wp_rest.list(list_arguments)
        if list_result.status_code in [200, 201]:
            try:
                result_dict = json.loads(list_result.text)
            except Exception:
                print("The response cant be mapped in a JSON, here it is in RAW")
                print(list_result.text)
                sys.exit(2)
            if arguments.verbosity >= 2:
                print(pretty_json(result_dict))
            else:
                for post in result_dict:
                    if arguments.verbosity == 0:
                        print('Post_id: {post_id} ; Title: {title} ; Link: {link}'.format(post_id=post['id'],
                                                                                          title=post['title']['rendered'],
                                                                                          link=post['link']))
                    else:
                        print('Post_id: {post_id} ; Title: {title} ; Link: {link}'.format(post_id=post['id'],
                                                                                          title=post['title']['rendered'],
                                                                                          link=post['link']))
                        print('Content: {content}'.format(content=post['content']['rendered']))
        else:
            print("There was a problem retreiving posts")
            print(list_result)
            print(list_result.text)

    # get
    if arguments.main_opt == 'get':
        result = wp_rest.get(arguments.blog_id)
        if result.status_code in [200, 201]:
            try:
                result_dict = json.loads(result.text)
            except Exception:
                print("The response cant be mapped in a JSON, here it is in RAW")
                print(result.text)
                sys.exit(2)
            if arguments.verbosity >= 2:
                print(pretty_json(result_dict))
            else:
                if arguments.verbosity == 0:
                    print('Post_id: {post_id} ; Title: {title} ; Link: {link}'.format(post_id=result_dict['id'],
                                                                                      title=result_dict['title'][
                                                                                          'rendered'],
                                                                                      link=result_dict['link']))
                else:
                    print('Post_id: {post_id} ; Title: {title} ; Link: {link}'.format(post_id=result_dict['id'],
                                                                                      title=result_dict['title'][
                                                                                          'rendered'],
                                                                                      link=result_dict['link']))
                    print('Content: {content}'.format(content=result_dict['content']['rendered']))
        else:
            print("There was a problem retreiving posts")
            print(result)
            print(result.text)

if __name__ == '__main__':
    main()