# DevOps take home assignment

Automating infrastructure and writing code to do it are among the main responsibilities of DevOps engineers in Image Intelligence. We want you to create a working WordPress installation inside a Vagrant box.

**We have the following pseudo-requirements:**

- Must talk to MySQL
- Able to handle 5000 requests per second, with 500 concurrent conns, on a current gen MacBook
  - We will use apache-bench, fetching a single post
- A WordPress local dev and prod environment
  - Deployed on AWS (you decide how you want it deployed)
- CLI tool (ideally in Python) that interacts with the WP install that can:
  - Create blog posts given an input JSON file with the post content (you choose the format)
  - Delete blog posts
  - Retrieve a single blog post
  - List blog posts

Please create a repository on GitHub or BitBucket with the following:

- A Vagrantfile for managing a virtual machine with a Linux-based operating system of your choice. Please use a publicly available box, for example one of the boxes hosted here: https://atlas.hashicorp.com/boxes/search
- A way to provision this Vagrant box. It can be whatever you want: sh scripts, Ansible playbooks etc. You must have a working Wordpress install talking to MySQL
- A README file that describes how to run it locally, how to use the CLI and how you deployed it to AWS

When all of this is complete, send us a link to your repository. Good luck!

--

Asumptions:
	Python is intalled :) (im using python 2.7). Python-dev has to be installed to install paramiko library(dependency of Fabric) because its needed to be compiled. Python setuptools, pip, virtualenv...
		Dev: debug active and https deactive
		Pro: debug deactive and http active (i have not implemented email, log rotation, other FS for logs, reverse proxyes for HA, etc.. i think is out of the scope of this exercise, is focused in Wordpress)

Amazon:
	Security group created: 80, 443, 3306... (depends of what to listen)
	key created: is setUp one mine, that is deprecated right now
	Im using t2.micro because its the free tier one
	VPC created
	Subnet created

I just create the instance.

Mysql: I always install a mysql, but its possible to specify don't install it (it has to be configured in somewhere)

Vagrant:
	Configuration is so easy, just 4 CPUs and 2048mb of Ram in a "Surface pro 3 i7" was enought to get 10000 p/s with 500 concurrent users
	The vagrant is autogenerate with the dame commands used for the instance of Amazon with the cli tool for deploy. It can be dev or pro too
	Bug: The plugin for Basic-auth has problem in the installation with Vagrant because a zip-corruption. Loged by ssh and do the same work, and in Amazon too. Dont know why that happen
	
CLI:
	The cli is a wrapper of the REST API V2, then, the JSON for create post is defined in the website
	Its prepared for OAuth, but it was impossible to get it (the code for the 3 legs is implemente3d). After the 3 legs, it never accept the signature
	I have tested the environment variables in Windows and in Windows-cygwin. I think in Mac is the same (hope so)
	
CLI Deploy:
	Checks of the JSON are not implemented, please, use as described
	
-----
Instructions:

Vagrant:
Just get into the vagrant folder and vagrant up
	Ports have to be availables, the same with the private ip. The machine has to be able to get 4CPUs and 2048. In windows there are problems with hyper-visor if its installed to virtualize more than one CPU, hope not in MAC.
	
CLI:
	it can be executed by: python cammy_python_cli/__init.py
	or
	Build and install the package: python setup.py sdist && pip install dist/*. And then use as a normal executable: wordpress_cli
	The instructions to configure are in the -h
	
CLI Deploy:
	It can be executed by: python cammy_python_deploy/__init.py
	or
	Build and install the package: python setup.py sdist && pip install dist/*. And then use as a normal executable: cammy_deploy. The parameter 'config_files_path' has to be a path where the three json are (The examples are in the 'json_configs' folder)
    
```
usage: cammy_deploy deploy [-h] [-v] config_files_path

positional arguments:
  config_files_path  Path with amazon_config.json, deploy.json and
                     files_dest.json

optional arguments:
  -h, --help         show this help message and exit
  -v, --verbosity    Show every command line executed and it resopnse

```
    Its possible to generate the Vagrant file with this cli, if something in the deploy change, a vagrant folder with the changes can be generated with this

```
usage: cammy_deploy vagrant [-h] [-v]
                            config_files_path CPU NUMBER RAM_MEMORY PATH

positional arguments:
  config_files_path  Path with amazon_config.json, deploy.json and
                     files_dest.json
  CPU NUMBER         Number of CPUs to configure to vagrant
  RAM_MEMORY         Amount of RAM to configure to vagrant
  PATH               Path where the Vagrantfile is going to be placed

optional arguments:
  -h, --help         show this help message and exit
  -v, --verbosity    Show every command line executed and it resopnse
```
	
    Configuration files for CLI Deploy:
	
deploy.json --> Here is defined how to deploy:
	
```	
{
  "ip": "192.168.33.12", --> Ip of a machine already working where to deploy all the stack
  "create_instance": "amazon", --if "amazon" is in "create_instance" it will create a new instance in Amazon. If it anything else it will try to install in the ip given before
  "install_mysql": true, --> If install mysql is needed. (Root password is 'wordpress')
  "db_username": "wordpress", --> The mysql data is for the installation and configuration in wordpress
  "db_password": "wordpress",
  "db_host": "localhost",
  "db_name": "wordpress",
  "env": "dev", --> It could be "dev" or "pro". Is not checked if its anythinelse
  "credentials": {
    "key_file_path": "C:\\Users\\cholas\\cammytest\\wordpress.pem", --> keyfile of the machine to deploy, amazon or not amazon
    "username": "ubuntu" --> because of the image i have chosen to amazon, the user has to be "ubuntu". For vagrant, the user has to be "vagrant"
  },
  "wordpress_title": "AutoDeploy", --> Title of the blog
  "wordpress_admin_user": "root", --> User admin
  "wordpress_admin_password": "root", --> user password
  "wordpress_admin_email": "root@example.com" --> Email
}
```

amazon_config.json --> Here is the configuration example (keys are deprecated). Both are the same because im in a free tier :):

```
{
  "dev": {
    "access_key": "AKIAIVIJLUXGH5KPCU6A",
    "secret_access_key": "wXprJAQf3IQSZZV+2TPKxoSx60xFLJY0Mpi0LVzJ",
    "image_id": "ami-bf3d00dc",
    "keyname": "wordpress",
    "security_group_id": "sg-740b1310",
    "instance_type": "t2.micro",
    "disc_size_gb": 20
  },
  "pro": {
    "access_key": "AKIAIVIJLUXGH5KPCU6A",
    "secret_access_key": "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",
    "image_id": "ami-bf3d00dc",
    "keyname": "wordpress",
    "security_group_id": "sg-740b1310",
    "instance_type": "t2.micro",
    "disc_size_gb": 40
  }
}

```

files_dest.json

```
{
  "nginx_conf": {
    "dest": "/etc/nginx/nginx.conf",
    "owner": "root"
  },
  "nginx_wordpress": {
    "dest": "/etc/nginx/sites-available/default",
    "owner": "root"
  },
  "php_fpm_pool_wwwconf": {
    "dest": "/etc/php5/fpm/pool.d",
    "owner": "root"
  },
  "php_ini": {
    "dest": "/etc/php5/fpm/php.ini",
    "owner": "root"
  },
  "wp_config": {
    "dest": "/var/www/wp-config.php",
    "owner": "www-data"
  }
}
```