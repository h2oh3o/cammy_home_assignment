echo "Installing Mysql"
export DEBIAN_FRONTEND=noninteractive && apt-get -q update && apt-get -q -y install mysql-server
echo "Configuring Mysql with WordPress"
mysqladmin -u root password 'wordpress'
mysqladmin -f -u root -pwordpress drop wordpress
mysql -uroot -pwordpress -e "DROP USER 'wordpress'@'%'"
mysqladmin -f -u root -pwordpress flush-privileges
mysqladmin -f -u root -pwordpress reload
mysqladmin -u root -pwordpress create wordpress &&service mysql restart &&mysql -uroot -pwordpress -e "CREATE USER 'wordpress'@'%' IDENTIFIED BY 'wordpress'" &&mysql -uroot -pwordpress -e "GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpress'@'%'" 
echo "Installing PHP"
export DEBIAN_FRONTEND=noninteractive && apt-get -q update && apt-get -q -y install php5-mysql php5-fpm php5-cli
echo "Installing Nginx"
export DEBIAN_FRONTEND=noninteractive && apt-get -q update && apt-get -q -y install nginx
echo "Installing WordPress"
wget -q https://wordpress.org/latest.tar.gz -O /tmp/wp.tar.gz && tar -zxf /tmp/wp.tar.gz -C /tmp/ && mkdir -p /var/www &&cp -Rf /tmp/wordpress/* /var/www/.
echo "Configuring the installations for a development environment"
echo "SSL Off"
echo "Debug On"
service nginx restart
mv /vagrant/nginx_conf /etc/nginx/nginx.conf && sudo chown -R root: /etc/nginx/nginx.conf && sudo chmod 644 /etc/nginx/nginx.conf
mv /vagrant/nginx_wordpress /etc/nginx/sites-available/default && sudo chown -R root: /etc/nginx/sites-available/default && sudo chmod 644 /etc/nginx/sites-available/default
mv /vagrant/php_fpm_pool_wwwconf /etc/php5/fpm/pool.d && sudo chown -R root: /etc/php5/fpm/pool.d && sudo chmod 644 /etc/php5/fpm/pool.d
mv /vagrant/php_ini /etc/php5/fpm/php.ini && sudo chown -R root: /etc/php5/fpm/php.ini && sudo chmod 644 /etc/php5/fpm/php.ini
mv /vagrant/wp_config /var/www/wp-config.php && sudo chown -R www-data: /var/www/wp-config.php && sudo chmod 644 /var/www/wp-config.php
service nginx restart

echo "Post-Install"
echo "Installing CLI"
wget -q https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar && chmod +x wp-cli.phar && mv wp-cli.phar /usr/local/bin/wp 
echo "Initializing wordpress"
sudo -u ubuntu -i -- wp core install --url=192.168.33.12 --title=AutoDeploy --admin_user=root --admin_password=root --admin_email=root@example.com --path="/var/www"
echo "Installing REST plugin"
chown -R ubuntu: /var/www/wp-content
sudo -u ubuntu -i -- wp plugin install rest-api --activate --path="/var/www"
chown -R www-data: /var/www/wp-content
echo "Installing HTTP auth plugin"
chown -R ubuntu: /var/www
sudo -u ubuntu -i -- wp plugin install https://github.com/WP-API/Basic-Auth/archive/master.zip --activate --path="/var/www"
chown -R www-data: /var/www
echo "Restart Nginx"
service nginx restart
echo "The deploy has finished."
echo "f a Mysql instance is installed, the password for the root user is 'wordpress"

