# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Python imports
import json
import ntpath
from os.path import join
from codecs import open
# Third party imports
from jinja2 import Template
# Local imports
from templates import TEMPLATES_HERE
from templates_tmp import TEMPLATES_TMP_HERE

__author__ = 'jon.caldering@gmail.com'


def path_leaf(filepath):
    """
    Get a file from a path, if it have just a slash in the end, take the previous path
    :param filepath:
    :return:
    """
    head, tail = ntpath.split(filepath)
    return tail or ntpath.basename(head)


def get_dest_and_owner_from_config(config_files_path, filename):
    """
    From a file name in template, give the dest configured in files_dest.json
    :param filename:
    :return:
    """
    config = json.load(open(join(config_files_path, 'files_dest.json'), encoding='utf-8'))
    dest = config.get(filename)
    if dest is None:
        print(
            'The destination for {} is not configured. Please configure it in "files_dest.json" file'.format(
                filename))
        return None
    else:
        return dest["dest"].encode('utf-8'), dest["owner"]


def get_template(filename):
    """
    Get the template given it file name
    :param filename:
    :return:
    """
    with open(join(TEMPLATES_HERE, filename), 'r', encoding='utf-8') as wp_config_template:
        return Template(wp_config_template.read())


def write_tmp_file(filename, content):
    """
    Write the contestn given in the name given in the template tmp directory
    :param filename:
    :param content:
    :return:
    """
    file_path = join(TEMPLATES_TMP_HERE, filename)
    with open(file_path, 'w', encoding='utf-8') as tmp_file:
        tmp_file.write(content)
    return file_path