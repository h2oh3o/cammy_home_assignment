# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Python imports
import json
import sys
import textwrap
import time
from os.path import join, exists
from os import listdir, makedirs
from shutil import copyfile
from codecs import open
import argparse
# Third party imports

# Local imports
from amazon_manage import AmazonManage
from deploys import Deploys
from templates_tmp import TEMPLATES_TMP_HERE
from tools import get_dest_and_owner_from_config

__author__ = 'jon.caldering@gmail.com'


def get_config(file_path):
    with open(join(file_path, 'deploy.json'), 'r') as config_file:
        try:
            config = json.load(config_file)
        except Exception:
            print("there is a problem with the json format of the deploy.json file")
            sys.exit(2)
        return config


def get_amazon_def(file_path):
    with open(join(file_path, 'amazon_config.json'), 'r') as config_file:
        try:
            config = json.load(config_file)
        except Exception:
            print("there is a problem with the json format of the deploy.json file")
            sys.exit(2)
        return config


def print_log(text, get_bash, deploy_manager=None, debug=True):
    if not get_bash:
        print(text)
    else:
        echo = "echo \"{}\"".format(text)
        if debug:
            print(echo)
        if deploy_manager is not None:
            deploy_manager.insert_in_bash(echo)


def process_deploy(config_files_path, debug=False, get_bash=False, cpu_cores=None, total_memory=None):
    """
    This take into account that a security group is already created
    :return:
    """
    deploy = get_config(config_files_path)
    instance = None
    create_instance = deploy.get("create_instance")
    if create_instance == "amazon" and get_bash is False:
        print_log("Creating instance in Amazon Ec2", get_bash, debug)
        amazon_def = get_amazon_def(config_files_path).get(deploy.get('env'))
        amazon = AmazonManage(amazon_def.get('access_key'), amazon_def.get('secret_access_key'))
        opts = {
            "ImageId": amazon_def.get('image_id'),
            "MinCount": 1,
            "MaxCount": 1,
            "KeyName": amazon_def.get('keyname'),
            "SecurityGroupIds": [amazon_def.get('security_group_id'), ],
            "InstanceType": amazon_def.get('instance_type'),
            "BlockDeviceMappings": [
                {
                    'VirtualName': 'disc1',
                    'DeviceName': '/dev/sdb',
                    'Ebs': {
                        'VolumeSize': amazon_def.get('disc_size_gb'),
                        'DeleteOnTermination': False,
                        'VolumeType': 'standard',
                        'Encrypted': False
                    },
                },
            ],
            "DisableApiTermination": False
        }
        instance = amazon.run_instances(**opts)[0]
        timeout = 300
        current_time_waited = 0
        print_log("Waiting for amazon instance to wake up (check every 30s for 5 minutes)", get_bash, debug)
        while current_time_waited < timeout:
            print("Checking...")
            if amazon.is_instance_running(instance):
                break
            else:
                current_time_waited += 30
                time.sleep(30)
        ip = amazon.get_public_ip(instance)
    else:
        ip = deploy.get('ip')
    deploy_manager = Deploys(ip, deploy.get('credentials').get('username'),config_files_path,
                             pub_id_rsa_path=deploy.get('credentials').get('key_file_path'), debug=debug,
                             get_bash=get_bash)
    # Mysql
    if deploy.get('install_mysql') is True:
        print_log("Installing Mysql", get_bash, deploy_manager, debug)
        deploy_manager.install_mysql()
        print_log("Configuring Mysql with WordPress", get_bash, deploy_manager, debug)
        deploy_manager.configure_mysql_for_wordpress(deploy.get('db_name'), deploy.get('db_username'),
                                                     deploy.get('db_password'), True)
        # PHP
        print_log("Installing PHP", get_bash, deploy_manager, debug)
    deploy_manager.install_php5()
    # Nginx
    print_log("Installing Nginx", get_bash, deploy_manager, debug)
    deploy_manager.install_nginx()
    # Wordpress
    print_log("Installing WordPress", get_bash, deploy_manager, debug)
    deploy_manager.install_wordpress()
    # Configue env
    if cpu_cores is None:
        print_log("Getting CPU cores", get_bash, deploy_manager, debug)
        cpu_cores = deploy_manager.get_number_of_cores()
    if total_memory is None:
        print_log("Getting memory", get_bash, deploy_manager, debug)
        total_memory = deploy_manager.get_total_memory()
    if deploy.get('env') == "pro":
        print_log("Configuring the installations for a productions environment", get_bash, deploy_manager, debug)
        print_log("SSL On --> An autosigned certificated will be created with the public ip", get_bash,
                  deploy_manager)
        print_log("Debug Off", get_bash, deploy_manager, debug)
        # TODO; Offer configure the own certificate and the proper domain name
        deploy_manager.configure_wordpress('wordpress', 'wordpress', 'wordpress', 'localhost', ip, cpu_cores,
                                           total_memory, ssl=True, debug=False, log_path=None)
        if get_bash is False:
            deploy_manager.put_config_files()
        deploy_manager.gen_nginx_ssl_autosign(ip)
    else:
        print_log("Configuring the installations for a development environment", get_bash, deploy_manager, debug)
        print_log("SSL Off", get_bash, deploy_manager, debug)
        print_log("Debug On", get_bash, deploy_manager, debug)
        deploy_manager.configure_wordpress('wordpress', 'wordpress', 'wordpress', 'localhost', ip, cpu_cores,
                                           total_memory, ssl=False, debug=True, log_path=None)
        if get_bash is False:
            deploy_manager.put_config_files()
        deploy_manager.restart_nginx()

    print_log("Post-Install", get_bash, deploy_manager, debug)

    # CLI management
    print_log("Installing CLI", get_bash, deploy_manager, debug)
    deploy_manager.install_wordpress_cli()
    # Initialize wordpress
    print_log("Initializing wordpress", get_bash, deploy_manager, debug)
    deploy_manager.initialize_wordpress(deploy.get('credentials').get('username'),
                                        ip,
                                        deploy.get("wordpress_title"),
                                        deploy.get("wordpress_admin_user"),
                                        deploy.get("wordpress_admin_password"),
                                        deploy.get("wordpress_admin_email"))
    # Plugins
    print_log("Installing REST plugin", get_bash, deploy_manager, debug)
    deploy_manager.install_wordpress_rest_plugin(deploy.get('credentials').get('username'))
    print_log("Installing HTTP auth plugin", get_bash, deploy_manager, debug)
    deploy_manager.install_basic_auth_plugin(deploy.get('credentials').get('username'))
    print_log("Restart Nginx", get_bash, deploy_manager, debug)
    deploy_manager.restart_nginx()
    print_log("The deploy has finished.", get_bash, deploy_manager, debug)
    if instance:
        print_log("A new Amazon instance has been created with the id '{instance}' and the public ip '{ip}'".format(
            instance=instance, ip=ip), get_bash, deploy_manager, debug)
    print_log("f a Mysql instance is installed, the password for the root user is 'wordpress", get_bash, deploy_manager, debug)
    if get_bash:
        return deploy_manager.get_bash_commands()


def gen_vagrant_installation(cpus, memory, vagrant_path, config_files_path, debug=False):
    if not exists(vagrant_path):
        makedirs(vagrant_path)
    vagrant_file = """
# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.configure(2) do |config|
  config.vm.box = "ubuntu/trusty64"
  config.vm.network "forwarded_port", guest: 80, host: 8080
  config.vm.network "forwarded_port", guest: 81, host: 8081
  config.vm.network "forwarded_port", guest: 443, host: 4433
  config.vm.network "private_network", ip: "{private_ip}"
  config.vm.provider "virtualbox" do |vb|
     vb.memory = "{memory}"
     vb.cpus = {cpus}
     vb.customize ["modifyvm", :id, "--ioapic", "on"]
  end
  config.vm.provision "shell", inline: <<-SHELL
    sudo chmod +x /vagrant/deploy.sh
    sudo bash /vagrant/deploy.sh
  SHELL
end
        """
    mv_files = ""
    for file in listdir(TEMPLATES_TMP_HERE):
        if file.find('__') == -1:
            src = join(TEMPLATES_TMP_HERE, file)
            dest = join(vagrant_path, file)
            copyfile(src, dest)
            dest_vm, owner = get_dest_and_owner_from_config(config_files_path, file)
            mv_files += "mv /vagrant/{file} {dest_vm} && sudo chown -R {owner}: {dest_vm} && sudo chmod 644 {dest_vm}\n".format(
                file=file, dest_vm=dest_vm, owner=owner
            )
    mv_files += 'service nginx restart\n'
    deploy = get_config(config_files_path)
    private_ip = deploy.get("ip")
    with open(join(vagrant_path, 'Vagrantfile'), 'w', encoding='utf-8') as vagrantfile:
        vagrantfile.write(str(vagrant_file.format(private_ip=private_ip,
                                                  memory=memory,
                                                  cpus=cpus)))
    with open(join(vagrant_path, 'deploy.sh'), 'w', encoding='utf-8') as deploy_bash:
        bash = process_deploy(config_files_path, debug, True, cpus, memory)
        new_bash = ""
        for line in bash.split('\n'):
            if line.find('Post-Install') != -1:
                new_bash += mv_files + '\n'
            new_bash += line + '\n'
        deploy_bash.write(str(new_bash))


def check_deploy_json(path):
    """
    Do some checks in the deploy.json file
    :param path:
    :return:
    """
    filepath = join(path, 'deploy.json')
    if not exists(filepath):
        print("The file {} doesn't exist.".format(filepath))
        sys.exit(2)
    config = get_config(path)
    if config.get("create_instance") == "amazon":
        if config.get("credentials").get('username') != 'ubuntu':
            print("The user for an amazon installation has to be 'ubuntu'")
            sys.exit(2)
    key_file_path = config.get("credentials").get('key_file_path')
    if not exists(key_file_path):
        print("There is not keyfile in the path {}".format(key_file_path))
        sys.exit(2)
    if config.get("env") not in ["dev", "pro"]:
        print("The environment can only be 'dev' or 'pro'")
        sys.exit(2)
    if config.get("create_instance") == "amazon":
        if config.get("ip") != "":
            print("WARNING: If the create_instance is 'amaazon', the ip will be provided by the amazon instance")




def pretty_json(data):
    try:
        result = json.dumps(data, sort_keys=True, indent=4, separators=(',', ': '))
    except Exception:
        result = json.dumps(json.loads(data), sort_keys=True, indent=4, separators=(',', ': '))
    return result


class DefaultHelpParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write(bytes('error: {message}\n'.format(message=message).encode('utf-8')))
        self.print_help()
        sys.exit(2)

def main():
    global_parser = DefaultHelpParser(add_help=False, formatter_class=argparse.RawDescriptionHelpFormatter,
                                      description=textwrap.dedent('''\
    Deploy wordpress
        Edit file "deploy.json:
            If create_instance has the value "amazon" it will create a instance in amazon.
            If create_instance has another value, it will just deploy Wordpress and the paremeter ip has to be provided.
            Only login with keyfile is allowed.
            If the installation is in Amazon, the user in the credentials has to be "ubuntu".
            If the deploy is used to gen Vagrant file, the user in "credentials" has to be "vagrant"
      '''))
    # Global options
    global_parser.add_argument("-v", "--verbosity", action="store_true", help="Show every command line executed and it resopnse",
                               required=False)
    global_parser.add_argument("config_files_path", help="Path with amazon_config.json, deploy.json and files_dest.json")

    # Subparsers
    parser = DefaultHelpParser()
    subparsers = parser.add_subparsers(dest="main_opt")

    # Deploy
    parser_deploy = subparsers.add_parser('deploy', help="Deploy wordpress with the data in deploy.json", parents=[global_parser])

    # Vagrant
    parser_vagrant = subparsers.add_parser('vagrant', help="Generate a Vagrant file with the data in deploy.json", parents=[global_parser])

    parser_vagrant.add_argument('cpus', metavar="CPU NUMBER", type=int, help="Number of CPUs to configure to vagrant")
    parser_vagrant.add_argument('memory', metavar="RAM_MEMORY", type=int, help="Amount of RAM to configure to vagrant")
    parser_vagrant.add_argument('vagrant_path', metavar="PATH", type=str, help="Path where the Vagrantfile is going to be placed")

    arguments = parser.parse_args()
    check_deploy_json(arguments.config_files_path)
    if arguments.main_opt == "deploy":
        if arguments.verbosity:
            process_deploy(arguments.config_files_path, True)
        else:
            process_deploy(arguments.config_files_path, False)
    elif arguments.main_opt == "vagrant":
        if arguments.verbosity:
            gen_vagrant_installation(arguments.cpus, arguments.memory, arguments.vagrant_path, arguments.config_files_path, True)
        else:
            gen_vagrant_installation(arguments.cpus, arguments.memory, arguments.vagrant_path, arguments.config_files_path, False)


if __name__ == '__main__':
    main()