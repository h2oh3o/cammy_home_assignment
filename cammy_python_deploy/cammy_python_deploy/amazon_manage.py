# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Python imports
import sys
import json
# Third party imports
import boto3

# Local imports

__author__ = 'jon.caldering@gmail.com'


def pretty_json(data):
    try:
        result = json.dumps(data, sort_keys=True, indent=4, separators=(',', ': '))
    except Exception:
        result = json.dumps(json.loads(data), sort_keys=True, indent=4, separators=(',', ': '))
    return result


class AmazonManage(object):
    def __init__(self, access_key, secret_access_key, deploy_users_group="cammy_deploy_users", region="ap-southeast-2"):
        self.access_key = access_key
        self.secret_access_key = secret_access_key
        self.region = region
        #TODO: Add check of the user rights and groups would be better
        self.deploy_users_group = deploy_users_group
        self.ec2_client = self.__get_ec2_client()

    def __get_ec2_client(self):
        return boto3.client('ec2', aws_access_key_id=self.access_key, aws_secret_access_key=self.secret_access_key,
                            region_name=self.region)

    def create_vpc(self):
        pass

    def create_subnet(self):
        pass

    def create_iam_role(self):
        pass

    def create_security_group(self):
        pass

    def run_instances(self, **kwargs):
        """
        Wrapper of the boto3 function. Cammy checks or automatic creation of resources that doesn't exist would be here
        :param kwargs:
        :return:
        """
        response = self.ec2_client.run_instances(**kwargs)
        id_list = []
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            for instance in response['Instances']:
                id_list.append(instance['InstanceId'])
            return id_list
        else:
            print("There was an error creating the instance in Amazon. Here is the response")
            print(response)
            sys.exit(2)

    def is_instance_running(self, id):
        response =self.ec2_client.describe_instance_status(InstanceIds=[id])
        if len(response["InstanceStatuses"]) == 0:
            return False
        instance = response["InstanceStatuses"][0]
        try:
            if instance["InstanceState"]['Name'] != "running" or instance["InstanceStatus"]["Status"] != "ok" or instance["SystemStatus"]["Status"] != "ok":
                return False
            else:
                return True
        except Exception:
            print("There is a problem getting the status of the instance, The result is:\n {}".format(response))
            sys.exit(2)

    def get_public_ip(self, id):
        response = self.ec2_client.describe_instances(InstanceIds=[id])
        return response.get("Reservations")[0].get("Instances")[0].get('PublicIpAddress')





if __name__ == '__main__':
    aws = AmazonManage("AKIAIVIJLUXGH5KPCU6A", "wXprJAQf3IQSZZV+2TPKxoSx60xFLJY0Mpi0LVzJ")
    opts = {
        "ImageId": 'ami-bf3d00dc',
        "MinCount": 1,
        "MaxCount": 1,
        "KeyName": 'wordpress',
        "SecurityGroupIds": ['sg-740b1310', ],
        "InstanceType": 't2.micro',
        "BlockDeviceMappings": [
            {
                'VirtualName': 'disc1',
                'DeviceName': '/dev/sdb',
                'Ebs': {
                    'VolumeSize': 20,
                    'DeleteOnTermination': False,
                    'VolumeType': 'standard',
                    'Encrypted': False
                },
            },
        ],

        "DisableApiTermination": False
    }
    res = aws.get_public_ip('i-095f8ca6cdd74f9ac')
    import pprint

    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(res)
