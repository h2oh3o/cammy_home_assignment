# You may add here your
# server {
#	...
# }
# statements for each of your virtual hosts to this file

##
# You should look at the following URL's in order to grasp a solid understanding
# of Nginx configuration files in order to fully unleash the power of Nginx.
# http://wiki.nginx.org/Pitfalls
# http://wiki.nginx.org/QuickStart
# http://wiki.nginx.org/Configuration
#
# Generally, you will want to move this file somewhere, and start with a clean
# file but keep this around for reference. Or just disable in sites-enabled.
#
# Please see /usr/share/doc/nginx-doc/examples/ for more detailed examples.
##

#   FastCGI cache setup
#fastcgi_cache_path /var/run/nginx-cache levels=1:2 keys_zone=WORDPRESS:100m inactive=60m;
fastcgi_cache_path /dev/shm/nginxcache levels=1:2 keys_zone=WORDPRESS:1m inactive=1m;
fastcgi_cache_key "$scheme$request_method$host$request_uri";
#   Determines in which cases a stale cached response can be used when an error occurs during communication with the FastCGI server
fastcgi_cache_use_stale error timeout invalid_header http_500;
#   WordPress themes (especially Photocrati) often send back inappropriate headers, so ignore them
fastcgi_ignore_headers Cache-Control Expires Set-Cookie;

# Deactivate http
#server {
#    listen 80;
#    location / {
#        return 301 https://$host$request_uri;
#    }
#}

server {
    #listen 80 default_server;
	#listen 80  spdy default_server;
	#listen 80  default_server;
	listen 80  default_server;
	listen [::]:80 default_server ipv6only=on;

    

    #   Path where is the WordPress
	root /var/www;
	index index.php index.html index.htm;

	#   Make site accessible from http://localhost/
	server_name 192.168.33.12;

	#   Logs
	access_log /var/log/nginx/access.log;
    error_log  /var/log/nginx/error.log;

    #   Do not log access to robots.txt, to keep the logs cleaner
    location = /robots.txt {
        access_log off;
        log_not_found off;
    }

    #   Do not log access to the favicon, to keep the logs cleaner
    location = /favicon.ico {
        access_log off;
        log_not_found off;
    }

    #   General Configuration
    location / {
        # First attempt to serve request as file, then
        # as directory, then fall back to displaying a 404.
        #try_files $uri $uri/ =404;
        # Pretty permalink
        try_files $uri $uri/ /index.php?$args;
    }
    #  Rules to work out when cache should/shouldn't be used
    set $skip_cache 0;

    # POST requests and URLs with a query string should always go to PHP
    if ($request_method = POST) {
        set $skip_cache 1;
    }

    if ($query_string != "") {
        set $skip_cache 1;
    }

    # Don't cache URIs containing the following segments
    if ($request_uri ~* "/wp-admin/|/xmlrpc.php|wp-.*.php|/feed/|index.php
                         |sitemap(_index)?.xml") {
        set $skip_cache 1;
    }

    # Don't use the cache for logged-in users or recent commenters
    if ($http_cookie ~* "comment_author|wordpress_[a-f0-9]+|wp-postpass
        |wordpress_no_cache|wordpress_logged_in") {
        set $skip_cache 1;
    }

    #   Pass the PHP scripts to FastCGI server listening on the php-fpm socket
    location ~ \.php$ {
            try_files $uri =404;
            fastcgi_pass unix:/var/run/php5-fpm.sock;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include fastcgi_params;
            fastcgi_read_timeout 10s;
            fastcgi_buffer_size 128k;
            fastcgi_buffers 256 16k;
            fastcgi_busy_buffers_size 256k;
            fastcgi_temp_file_write_size 256k;
            #   FastCGI cache
            fastcgi_cache WORDPRESS;
            fastcgi_cache_valid 200 1m;
    }

    #    Add trailing slash to */wp-admin requests so the admin interface works correctly
    rewrite /wp-admin$ $scheme://$host$uri/ permanent;

    #   Location of the files uploaded by users. Usually other FS
    location /wp-content {
        alias /tmp/content/;
    }

    location /wp-content/plugins {
        alias /tmp/plugins/;
    }

    #   Restrictions

    #   Give less information
    error_page 403 =404;

	#   Prevent access to any files starting with a dot, like .htaccess or text editor temp files
    location ~ /\. {
        access_log off;
        log_not_found off;
        deny all;
    }

    #   Prevent access to any files starting with a $ (usually temp files)
    location ~ ~$ {
        access_log off;
        log_not_found off;
        deny all;
    }

    #   No other cgi scripts
    location ~* .(pl|cgi|py|sh|lua)$ {
        return 404;
    }

    #   Common deny or internal locations, to help prevent access to areas of the site that should not be public
    location ~* wp-admin/includes {
        deny all;
    }
    location ~* wp-includes/theme-compat/ {
        deny all;
    }
    location ~* wp-includes/js/tinymce/langs/.*\.php {
        deny all;
    }

    #   Prevent any potentially-executable files in the uploads directory from being executed by forcing their MIME type to text/plain
    location ~* ^//wp-content/uploads/.*.(html|htm|shtml|php)$ {
        types { }
        default_type text/plain;
    }

    #   Deny public access to wp-config.php
    location ~* wp-config.php {
        deny all;
    }

    # Restrict access to WordPress dashboard (network is unknown)
    #location /wp-admin {
    #    deny  192.192.9.9;
    #    allow 192.192.1.0/24;
    #    allow 10.1.1.0/16;
    #    deny  all;
    #}

    # Performance

    #   Keep images, CSS and other static files around in browser cache for as long as possible, to cut down on server load
    location ~* .(ogg|ogv|svg|svgz|eot|otf|woff|mp4|ttf|css|rss|atom|js|jpg|jpeg|gif|png|ico|zip|tgz|gz|rar|bz2|doc|xls|exe|ppt|tar|mid|midi|wav|bmp|rtf)$ {
        expires max;
        log_not_found off;
        access_log off;
    }

}