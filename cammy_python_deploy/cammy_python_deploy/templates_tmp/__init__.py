# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Python imports
import os
# Third party imports

# Local imports

__author__ = 'jon.caldering@gmail.com'

TEMPLATES_TMP_HERE = os.path.dirname(os.path.realpath(__file__))