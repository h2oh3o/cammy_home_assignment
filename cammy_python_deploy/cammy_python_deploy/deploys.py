# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Python imports
from os import listdir
# Third party imports
from fabric.api import run, env, sudo, put
from fabric.context_managers import settings, hide

# Local imports
from tools import *

__author__ = 'jon.caldering@gmail.com'


class Deploys(object):
    tmp_sufix = '_tmp'

    def __init__(self, ip, user, config_files_path, pub_id_rsa_path=None, debug=False, get_bash=False, ):
        env.host_string = ip
        self.debug = debug
        self.get_bash = get_bash
        self.config_files_path = config_files_path
        self.bash = ""
        if pub_id_rsa_path is not None:
            env.key_filename = pub_id_rsa_path
        env.user = user

    # Utils

    def __run(self, command, as_sudo=False, sudo_user=None):
        if self.get_bash:
            # if as_sudo:
            #     command_exec = "sudo {}".format(command)
            #     self.bash += command_exec + '\n'
            #     print(command_exec)
            # else:
                res = command + '\n'
                self.bash += res
                if self.debug:
                    print(command)
        else:
            if as_sudo:
                if not self.debug:
                    with settings(hide('warnings', 'running', 'stdout', 'stderr', 'user')):
                        if sudo_user is not None:
                            return sudo(command, sudo_user)
                        else:
                            return sudo(command)
                else:
                    if sudo_user is not None:
                        return sudo(command, user=sudo_user)
                    else:
                        return sudo(command)
            else:
                if not self.debug:
                    with settings(hide('warnings', 'running', 'stdout', 'stderr', 'user')):
                        return run(command)
                else:
                    return run(command)

    def __set_owner(self, owner, path):
        """
        Change owner and group of a path
        :param owner:
        :param path:
        :return:
        """
        self.__run('chown -R {owner}: {path}'.format(owner=owner, path=path), True)

    def __put_file(self, local_file_path):
        """
        Put a template in the remote destination
        :param local_file_path:
        :return:
        """
        dest, owner = get_dest_and_owner_from_config(self.config_files_path, path_leaf(local_file_path))
        put(local_file_path, dest, True)
        self.__set_owner(owner, dest)

    def get_number_of_cores(self):
        """
        Get the number of cores of the destiation
        :return:
        """
        return self.__run('nproc', True)

    def get_total_memory(self):
        """
        Get the total memory of the destination
        :return:
        """
        return int(self.__run("awk '/MemTotal/ {print $2}' /proc/meminfo", True)) / 1024

    def insert_in_bash(self, command):
        self.bash += command + '\n'

    def get_bash_commands(self):
        return self.bash

    # Installations

    def install_mysql(self):
        """
        Install mysql-server
        :return:
        """
        self.__run('export DEBIAN_FRONTEND=noninteractive && apt-get -q update && apt-get -q -y install mysql-server', True)

    def install_nginx(self):
        """
        Instal Nginx webserver
        :return:
        """
        # TODO; Compile from source to add debug
        self.__run('export DEBIAN_FRONTEND=noninteractive && apt-get -q update && apt-get -q -y install nginx', True)

    def install_php5(self):
        """
        Install php-fpm
        :return:
        """
        self.__run(
            'export DEBIAN_FRONTEND=noninteractive && apt-get -q update && apt-get -q -y install php5-mysql php5-fpm php5-cli',
            True)

    def install_wordpress(self):
        """
        Download latast wordpress and install it
        :return:
        """
        self.__run("wget -q https://wordpress.org/latest.tar.gz -O /tmp/wp.tar.gz && "
                   "tar -zxf /tmp/wp.tar.gz -C /tmp/ && "
                   "mkdir -p /var/www &&"
                   "cp -Rf /tmp/wordpress/* /var/www/."
                   , True)

    def install_wordpress_cli(self):
        """
        Install wordpress cli management tool
        :return:
        """
        self.__run("wget -q https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar && "
                   "chmod +x wp-cli.phar && "
                   "mv wp-cli.phar /usr/local/bin/wp "
                   , True)

    # Configurations

    def configure_mysql_for_wordpress(self, db_name='wordpress', user='wordpress', password='wordpress',
                                      delete_db=False):
        """
        Configure mysql to work with wordpress
        :param db_name:
        :param user:
        :param password:
        :param delete_db:
        :return:
        """
        env.warn_only = True
        self.__run("mysqladmin -u root password 'wordpress'", True)
        if delete_db:
            self.__run("mysqladmin -f -u root -pwordpress drop {}".format(db_name), True)
            self.__run("mysql -uroot -pwordpress -e \"DROP USER '{}'@'%'\"".format(user), True)
            self.__run("mysqladmin -f -u root -pwordpress flush-privileges", True)
            self.__run("mysqladmin -f -u root -pwordpress reload", True)
        env.warn_only = False
        self.__run("mysqladmin -u root -pwordpress create {db_name} &&"
                   "service mysql restart &&"
                   "mysql -uroot -pwordpress -e \"CREATE USER '{user}'@'%' IDENTIFIED BY '{password}'\" &&"
                   "mysql -uroot -pwordpress -e \"GRANT ALL PRIVILEGES ON {db_name}.* TO '{user}'@'%'\" "
                   .format(db_name=db_name, user=user, password=password)
                   , True)

    @staticmethod
    def configure_wp_config_file(db_name='wordpress', db_user='wordpress', db_password='wordpress',
                                 db_host='localhost',
                                 ssl='false',
                                 debug='false', debug_log='false', debug_display='false', script_debug='false',
                                 save_queries='false',
                                 memory_limit='128', max_memory_limit='512',
                                 cache='false', compress_css='false', compress_scripts='false',
                                 concatenate_scripts='false', force_gzip='false',
                                 auto_update='false', disable_mod_updates='false', disable_edit_updates='false',
                                 content_path='/var/www/wp-content', uri_content_path='/wp-content',
                                 plugins_path='/var/www/wp-content/plugins', uri_plugins_path='/wp-content/plugins',
                                 scheme='http'):
        """
        Set up wp-config.php from the template
        :param db_name:
        :param db_user:
        :param db_password:
        :param db_host:
        :param ssl:
        :param debug:
        :param debug_log:
        :param debug_display:
        :param script_debug:
        :param save_queries:
        :param memory_limit:
        :param max_memory_limit:
        :param cache:
        :param compress_css:
        :param compress_scripts:
        :param concatenate_scripts:
        :param force_gzip:
        :param auto_update:
        :param disable_mod_updates:
        :param disable_edit_updates:
        :param content_path:
        :param uri_content_path:
        :param plugins_path:
        :param uri_plugins_path:
        :param scheme:
        :return:
        """
        filename = 'wp_config'
        template = get_template(filename)
        file_wrote = write_tmp_file(filename,
                                    template.render(db_name=db_name, db_user=db_user, db_password=db_password,
                                                    db_host=db_host,
                                                    ssl=ssl,
                                                    debug=debug, debug_log=debug_log,
                                                    debug_display=debug_display,
                                                    script_debug=script_debug,
                                                    save_queries=save_queries,
                                                    memory_limit=memory_limit, max_memory_limit=max_memory_limit,
                                                    cache=cache, compress_css=compress_css,
                                                    compress_scripts=compress_scripts,
                                                    concatenate_scripts=concatenate_scripts,
                                                    force_gzip=force_gzip,
                                                    auto_update=auto_update,
                                                    disable_mod_updates=disable_mod_updates,
                                                    disable_edit_updates=disable_edit_updates,
                                                    content_path=content_path, uri_content_path=uri_content_path,
                                                    plugins_path=plugins_path,
                                                    uri_plugins_path=uri_plugins_path, scheme=scheme))
        return file_wrote

    @staticmethod
    def configure_nginx_site_config(listen_port='80', listen_ssl=True,
                                    root_path='/var/www', host_name='localhost',
                                    log_access_path='/var/log/nginx/access.log',
                                    log_error_path='/var/log/nginx/error.log', content_alias_filesystem='/tmp/content/',
                                    content_uri='/wp-content', plugins_uri='/wp-content/plugins',
                                    plugin_alias_filesystem='/tmp/plugins/'
                                    ):
        """
        Set up sites-availables/default from the template
        :param listen_port:
        :param listen_ssl:
        :param root_path:
        :param host_name:
        :param log_access_path:
        :param log_error_path:
        :param content_alias_filesystem:
        :param content_uri:
        :param plugins_uri:
        :param plugin_alias_filesystem:
        :return:
        """
        filename = 'nginx_wordpress'
        template = get_template(filename)
        file_wrote = write_tmp_file(filename,
                                    template.render(listen_port=listen_port, listen_ssl=listen_ssl,
                                                    root_path=root_path, host_name=host_name,
                                                    log_access_path=log_access_path,
                                                    log_error_path=log_error_path,
                                                    content_alias_filesystem=content_alias_filesystem,
                                                    content_uri=content_uri,
                                                    plugins_uri=plugins_uri,
                                                    plugin_alias_filesystem=plugin_alias_filesystem))
        return file_wrote

    @staticmethod
    def configure_global_nginx_config(worker_processes='4', worker_connections='8096',
                                      access_log_path='/var/log/nginx/access.log',
                                      error_log_path='/var/log/nginx/error.log'):
        """
        Set up nginx.conf from the template
        :param worker_processes:
        :param worker_connections:
        :param access_log_path:
        :param error_log_path:
        :return:
        """
        filename = 'nginx_conf'
        template = get_template(filename)
        file_wrote = write_tmp_file(filename, template.render(worker_processes=worker_processes,
                                                              worker_connections=worker_connections,
                                                              access_log_path=access_log_path,
                                                              error_log_path=error_log_path))
        return file_wrote

    @staticmethod
    def configure_php_ini():
        """
        Set up php.ini from the templates
        :return:
        """
        filename = 'php_ini'
        template = get_template(filename)
        file_wrote = write_tmp_file(filename, template.render())
        return file_wrote

    @staticmethod
    def configure_php_fpm_pool_wwwconf():
        """
        Set up php5/fpm/pool.d/www.conf from the templates
        :return:
        """
        filename = 'php_fpm_pool_wwwconf'
        template = get_template(filename)
        file_wrote = write_tmp_file(filename, template.render())
        return file_wrote

    def configure_wordpress(self, db_user, db_password, db_name, db_host, name_server, cpu_cores, memory_mbs,
                            ssl=False,
                            debug=True, log_path=None):
        """
        Configure environment to execute wordpress
        :param db_user:
        :param db_password:
        :param db_name:
        :param db_host:
        :param name_server:
        :param ssl:
        :param debug:
        :param log_path:
        :return:
        """
        cpu_cores = int(cpu_cores)
        memory_mbs = int(memory_mbs)
        # Set config arguments
        nginx_global_arguments = {}
        nginx_site_arguments = {
            'host_name': name_server
        }
        wp_config_arguments = {
            'db_name': db_name,
            'db_user': db_user,
            'db_password': db_password,
            'db_host': db_host
        }
        # Calcs
        nginx_global_arguments.update({'worker_processes': cpu_cores})
        if cpu_cores >= 4 or cpu_cores <= 0:
            worker_connections = 8096
        else:
            worker_connections = int((8096 / 4) * cpu_cores)
        nginx_global_arguments.update({'worker_connections': worker_connections})
        if memory_mbs >= 512:
            wp_max_memory = int(memory_mbs / 2)
            wp_max_memory_limit = int((memory_mbs / 4) * 3)
            # TODO: Change memory of PHP and study deactivate cache in a low memory (for docker test environment)
            wp_config_arguments.update({'memory_limit': wp_max_memory, 'max_memory_limit': wp_max_memory_limit})
        # Unify Logs
        if log_path is not None:
            # Global
            nginx_access_log = '{}/nginx_access.log'.format(log_path)
            nginx_error_log = '{}/nginx_error.log'.format(log_path)
            nginx_global_arguments.update({'access_log_path': nginx_access_log, 'error_log_path': nginx_error_log})
            # Site
            nginx_site_log_access_path = '{}/access.log'.format(log_path)
            nginx_site_log_error_path = '{}/error.log'.format(log_path)
            nginx_site_arguments.update({'log_access_path': nginx_site_log_access_path,
                                         'log_error_path': nginx_site_log_error_path})

            # TODO: Get php logs and wordpress debug.log file

        # SSL
        if ssl:
            nginx_site_arguments.update({
                'listen_port': '443',
                'listen_ssl': 'ssl',
            })
            wp_config_arguments.update({'ssl': 'true', 'scheme': 'https'})
        else:
            nginx_site_arguments.update({
                'listen_port': '80',
                'listen_ssl': '',
            })
            wp_config_arguments.update({'ssl': 'false'})

        # Debug
        if debug:
            wp_config_arguments.update({
                'debug': 'true',
                'debug_log': 'true',
                'debug_display': 'true',
                'script_debug': 'true',
                'save_queries': 'true'
            })
        else:
            wp_config_arguments.update({
                'debug': 'false',
                'debug_log': 'false',
                'debug_display': 'false',
                'script_debug': 'false',
                'save_queries': 'false'
            })
        # TODO: Config the user data directories in a different place (wp-content). In production it will probably be in other disk or FS
        self.configure_global_nginx_config(**nginx_global_arguments)
        self.configure_nginx_site_config(**nginx_site_arguments)
        self.configure_php_ini()
        self.configure_php_fpm_pool_wwwconf()
        self.configure_wp_config_file(**wp_config_arguments)

    def put_config_files(self):
        """
        Put the config files in temnplate_tmp directory in it dest
        :return:
        """
        template_files = listdir(TEMPLATES_TMP_HERE)
        for template in template_files:
            if template.find('__') == -1:
                self.__put_file(join(TEMPLATES_TMP_HERE, template))
                # # Nginx Files
                # self.__put_file()
                # self.__set_owner('root', self.__get_dest_from_config('nginx_conf'))
                # self.__put_file(self.configure_nginx_site_config(**nginx_site_arguments))
                # self.__set_owner('root', self.__get_dest_from_config('nginx_wordpress'))
                #
                # # PHP Files
                # self.__put_file(self.configure_php_ini())
                # self.__set_owner('root', self.__get_dest_from_config('php_ini'))
                # self.__put_file(self.configure_php_fpm_pool_wwwconf())
                # self.__set_owner('root', self.__get_dest_from_config('php_fpm_pool_wwwconf'))
                #
                # # wordpress Files
                # self.__put_file(self.configure_wp_config_file(**wp_config_arguments))
                # self.__set_owner('www-data', self.__get_dest_from_config('wp_config'))

    # Post Configurations

    def initialize_wordpress(self, user_who_install, url, title, admin_user, admin_password, admin_email):
        """
        Initialize wordpress
        :return:
        """
        self.__run(
            "sudo -u {user_who_install} -i -- wp core install "
            "--url={url} "
            "--title={title} "
            "--admin_user={admin_user} "
            "--admin_password={admin_password} "
            "--admin_email={admin_email} "
            "--path=\"/var/www\"".format(user_who_install=user_who_install,
                url=url,
                title=title,
                admin_user=admin_user,
                admin_password=admin_password,
                admin_email=admin_email))

    def install_basic_auth_plugin(self, user_who_installs):
        """
        Install plugin for basic authentication
        :param user_who_installs:
        :return:
        """
        self.__set_owner(user_who_installs, '/var/www')
        self.__run(
            "sudo -u {} -i -- wp plugin install "
            "https://github.com/WP-API/Basic-Auth/archive/master.zip --activate --path=\"/var/www\""
                .format(user_who_installs))
        self.__set_owner('www-data', "/var/www")

    def install_wordpress_rest_plugin(self, user_who_installs):
        """
        Install plugin for REST management
        :param user_who_installs:
        :return:
        """
        self.__set_owner(user_who_installs, '/var/www/wp-content')
        self.__run("sudo -u {} -i -- wp plugin install rest-api --activate --path=\"/var/www\""
                   .format(user_who_installs))
        self.__set_owner('www-data', '/var/www/wp-content')

    def restart_nginx(self):
        """
        Restart nginx webservice
        :return:
        """
        self.__run('service nginx restart', True)

    def gen_nginx_ssl_autosign(self, server_name):
        """
        Generate autosign certificates for nginx
        :param server_name:
        :return:
        """
        self.__run('mkdir -p /etc/nginx/ssl', True)
        commands = """
            openssl genrsa -des3 -passout pass:x -out server.pass.key 2048 &&
            openssl rsa -passin pass:x -in server.pass.key -out nginx.key &&
            rm server.pass.key &&
            openssl req -new -key nginx.key -out nginx.csr \
              -subj "/C=UK/ST=Warwickshire/L=Leamington/O=OrgName/OU=IT Department/CN={}" &&
            openssl x509 -req -days 365 -in nginx.csr -signkey nginx.key -out nginx.crt &&
            mv nginx.crt /etc/nginx/ssl/nginx.crt &&
            mv nginx.key /etc/nginx/ssl/nginx.key
            rm nginx.csr
        """.format(server_name)
        self.__run(commands, True)


if __name__ == '__main__':
    fab = Deploys('192.168.33.10', user='vagrant', pub_id_rsa_path="C:\\Users\\cholas\\cammytest\\vagrant\\private_key",
                  debug=True)
    # fab.configure_wordpress('wordpress', 'wordpress', 'wordpress', 'localhost', '13.54.140.114', cpu_cores=2, memory_mbs=1024, ssl=True, debug=True, log_path=None)
    # fab.restart_nginx()
    fab.install_basic_auth_plugin('vagrant')
